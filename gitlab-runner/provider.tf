provider "gitlab" {
    token = var.gitlab_token
}
provider "nomad" {
    address = var.address_nomad
}

provider "local" {
}