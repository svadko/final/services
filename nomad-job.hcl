job "gitlab-runner" {
  datacenters = ["dc1"]
  
  group "runner" {
    count = 1

    network {
        port "runner" {}
    }

    task "register-runner" {
      driver = "docker"

      config {
        image = "gitlab/gitlab-runner:latest"
        
        command = "register"
        
      }
      
      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}
