import paramiko
import schedule
import time


no_password=False
key_path='//root/.ssh/id_rsa'

def init_command(hostname, port, username, password, command):
    # Создание SSH-клиента
    ssh = paramiko.SSHClient()
    stdout=None
    
    if True:
        # Подключение к серверу
        if not no_password:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname, port=port, username=username, password=password)
            stdin, stdout, stderr = ssh.exec_command(command=command)
            # print(f"STDOUT:\n{stdout.read().decode()}")
            print(f"STDERR:\n{stderr.read().decode()}")
        else:
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            private_key = paramiko.RSAKey(filename=key_path)
            ssh.connect(hostname, port=port, username=username, pkey=private_key)
            stdin, stdout, stderr = ssh.exec_command(command=command)
            print(f"STDERR:\n{stderr.read().decode()}")

    return stdout.read().decode()

def init_command_log(hostname, port, username, password, command, log_dest):
    # Создание SSH-клиента
    ssh = paramiko.SSHClient()
    stdout=None
    output=""
    if True:
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(hostname, port=port, username=username, password=password)
        stdin, stdout, stderr = ssh.exec_command(command=command)
        if not no_password:
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            ssh.connect(hostname, port=port, username=username, password=password)
            stdin, stdout, stderr = ssh.exec_command(command=command)
            output = stdout.read().decode()
            print(f"STDOUT:\n{output}")
            print(f"STDERR:\n{stderr.read().decode()}")
        else:
            ssh.load_system_host_keys()
            ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            private_key = paramiko.RSAKey(filename=key_path)
            ssh.connect(hostname, port=port, username=username, pkey=private_key)
            stdin, stdout, stderr = ssh.exec_command(command=command)
            output = stdout.read().decode()
            print(f"STDOUT:\n{output}")
            print(f"STDERR:\n{stderr.read().decode()}")
    
    # Запись вывода в файл
    with open(log_dest, 'w') as file:
        file.write(output)

    return output


def read_hosts_from_file(file_path):
    with open(file_path, 'r') as file:
        return [line.strip() for line in file.readlines()]

if __name__ == "__main__":
    
    remote_command_2="echo | find {} -name '*.log' -exec cat -n {} + | sort -t $'\t' -k 1,1 | tail -n 20"
    remote_command_1="echo | find /var/log/ -type d"
    
    hosts=read_hosts_from_file("hosts.txt")
    print("Choice host:")
    for x in range(0,len(hosts)):
        print(x,")",hosts[x])

    remote_hostname = input("Remote hostname:")
    remote_hostname=hosts[int(remote_hostname)]
    
    while remote_hostname not in hosts:
        print("Error")
        remote_hostname = input("Remote hostname:")
        remote_hostname=hosts[int(remote_hostname)]
    
    remote_port=input("Remote port (default 22):")
    try:
        remote_port=int(remote_port)
    except:
        remote_port=22
        
    remote_password=input("Remote password:")
    if remote_password=="":
        no_password=True
    
    remote_username=input("Remote username:")
    
    if True: 
        logs=init_command(remote_hostname, remote_port, remote_username, remote_password, remote_command_1)
        # print(logs)
        logs=logs.split("\n")
        for x in range(0,len(logs)):
            print(x,")",logs[x])
            
        log_type=int(input("Select log type id: "))
        log_time=int(input("Select log delay in second: "))
        log_size=input("Select log size: ")
        log_file=input("Select log file dest: ")
        
        remote_command_2="echo | find " + logs[log_type] +" -name '*.log' -exec cat -n {} + | sort -t $'\t' -k 1,1 | tail -n"+log_size
        
        schedule.every(log_time).seconds.do(init_command_log,remote_hostname, remote_port, remote_username, remote_password, remote_command_2,log_file)

        while True:
            schedule.run_pending()
            time.sleep(1)
        # print("echo | find " + logs[log_type] +" -name '*.log' -exec cat -n {} + | sort -t $'\t' -k 1,1 | tail -n 20")