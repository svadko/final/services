# SvaDko

## Short Description
E-shop on which you can buy various books.

## Detailed Description


## Responsible People
20B030614 Keulimzhanov_Magzhan - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030498 Pirniyazov_Miras - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030587 Zhaksylykov_Nursultan - Nomad, Vault, GitLab Runner, Gitlab CI/CD<br />
21B030690 Kusanov_Ramzes - Nomad, Vault, GitLab Runner, Gitlab CI/CD, Logging and monitoring<br />
20B030668 Kaliev_Alikhan - Traefik Proxy, GitLab Runner, Gitlab CI/CD<br />

## Installation


## ToDo

1. Authentication and authorization mechanisms
2. Caching strategies for improving performance
3. API versioning and deprecation management

## Contributing
### Reporting Bugs/Issues
<ul>
<li>Search existing issues: Before reporting a new issue, please search for existing ones that may describe the same problem. This saves time and prevents duplicate work. <br>
<li>Use the issue template: We have a specific issue template to help you provide all the necessary information for us to effectively address the issue. Please fill it out as thoroughly as possible.<br>
<li>Be clear and concise: Describe the issue in a clear and concise way, including:<br>
<li>Steps to reproduce the issue: Provide detailed steps so we can easily replicate the problem.<br>
<li>Expected behavior: Describe what you expect to happen instead of what actually happens.<br>
<li>System information: Include details about your operating system, browser (if applicable), and any relevant software versions.<br>
<li>Screenshots/Video recordings: If possible, include screenshots or video recordings of the issue to make it easier to understand.<br>
</ul>

### Contributing Code
<ul>
<li>Fork the repository: Create your own fork of the repository before you start making changes. This allows you to work on your own branch without affecting the main codebase.<br>
<li>Follow coding standards: Please adhere to the project's coding standards to ensure consistency and maintainability. You can find the coding standards documented in the Coding Standards document: link to coding standards document.<br>
<li>Write unit tests: For any new code you write, please also write unit tests to ensure that your changes don't break anything.<br>
<li>Create a pull request: Once you're happy with your changes, create a pull request to the main repository. Please provide a clear and concise description of your changes in the pull request description.<br>
</ul>

