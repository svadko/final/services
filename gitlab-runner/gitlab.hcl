job "gitlab-runner" {
  datacenters = ["dc1"]

  group "runner" {
    task "runner" {
      driver = "docker"

      config {
        image = "gitlab/gitlab-runner:latest"

        volumes = [
          "/modules/runner-registration/config.toml:/etc/gitlab-runner",
        ]
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}