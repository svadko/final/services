variable "gitlab_group_id" {
    type = number
}
variable "gitlab_token" {
    type = string
}
variable "address_nomad"{
    type = string
}