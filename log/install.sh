#!/bin/bash

# Проверка наличия Python
if command -v python3 &>/dev/null; then
    echo "Python уже установлен"
else
    echo "Установка Python"
    sudo apt-get update
    sudo apt-get install -y python3
fi

apt install python3-pip
pip install schedule
pip install paramiko

# Завершение скрипта
echo "Скрипт завершен"
