job "traefik" {
  region      = "global"
  datacenters = ["dc1"]
  type        = "service"

  group "traefik" {
    count = 1

    network {
      port "http" {
        static = 8080
      }

      port "api" {
        static = 8081
      }
    }

    service {
      name = "traefik"
      check {
        name     = "alive"
        type     = "tcp"
        port     = "http"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "traefik" {
      driver = "docker"      

      config {
        image        = "traefik:v2.2"
        network_mode = "host"

        volumes = [
          "local/traefik.toml:/etc/traefik/traefik.toml",
          "config:/etc/traefik/config",
        ]
      }
      
      dynamic "template" {
        for_each = fileset("config", "*")

        content {
          destination = "local/${template.value}"
          data        = file("config/${template.value}")
        }
      }
        
    resources {
        cpu    = 100
        memory = 128
      }
    
    }


  }
}
