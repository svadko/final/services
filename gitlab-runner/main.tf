resource "gitlab_user_runner" "devrunner" {
  description = "Gitlab runner"
  runner_type = "group_type"
  access_level = "not_protected"
  group_id = var.gitlab_group_id
  locked = false
  paused = false
  tag_list = [ "nomad" ]
  untagged = true
  
}

resource "local_file" "gitlab_runner_config" {
  content = <<EOT
concurrent = 1
check_interval = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "test-runner"
  url = "https://gitlab.com"
  token = ${var.gitlab_token}
    executor = "docker"
  [runners.custom_build_dir]
  [runners.docker]
    tls_verify = false
    image = "gitlab/gitlab-runner:latest"
    privileged = true
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/modules/runner-registration/config.toml:/etc/gitlab-runner", "/cache"]
    shm_size = 0
  [runners.cache]
    [runners.cache.s3]
    [runners.cache.gcs]
EOT

  filename = "${path.module}/modules/runner-registration/config.toml"
}

resource "nomad_job" "app" {
  jobspec = file("${path.module}/gitlab.hcl")
}