terraform {
  required_providers {
    digitalocean = {
      source = "digitalocean/digitalocean"
      version = "~> 2.0"
    }
    gitlab = {
      source="gitlabhq/gitlab"
      version="16.6.0"
    }

    nomad = {
      source = "hashicorp/nomad"
      version = "2.0.0"
    }
  }
}