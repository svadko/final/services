# SvaDko
## Short Description
SvaDko is an online e-shop that offers a wide range of products. Our project leverages GitLab Runner for continuous integration and delivery (CI/CD) to automate the testing and deployment of code changes.<br>

## Detailed Description
Product Catalog: Browse and search for various products.<br>
User Authentication: Securely log in and manage your account.<br>
Order Processing: Place orders and track their status.<br>
Payment Gateway Integration: Seamless payment processing.<br>

## Responsible People
20B030614 Keulimzhanov_Magzhan - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030498 Pirniyazov_Miras - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030587 Zhaksylykov_Nursultan - Nomad, Vault, GitLab Runner, Gitlab CI/CD<br />
21B030690 Kusanov_Ramzes - Nomad, Vault, GitLab Runner, Gitlab CI/CD, Logging and monitoring<br />
20B030668 Kaliev_Alikhan - Traefik Proxy, GitLab Runner, Gitlab CI/CD<br />

## Installation
### Configure GitLab Runner:

Install GitLab Runner on your preferred infrastructure (self-managed or GitLab.com SaaS runners).<br>
Register the runner with your GitLab instance using the following command:<br>
gitlab-runner register

### Set Environment Variables:

Create a .env file with the following content:<br>
GITLAB_GROUP_ID=79699575<br>
GITLAB_TOKEN=glpat-yhNMrYMkCsMN3ATTqX7u<br>
ADDRESS_NOMAD=http://104.248.32.114:4646<br>

### Run Terraform:

Apply the Terraform configuration to set up your infrastructure:<br>
terraform apply -var="gitlab_group_id=79699575" -var="gitlab_token=glpat-yhNMrYMkCsMN3ATTqX7u" <br>
-var="address_nomad=http://104.248.32.114:4646"<br>

## TODO
Enhance the product recommendation engine.<br>
Implement real-time inventory tracking.<br>
Integrate with third-party shipping services.<br>

## Contributing
### Reporting Bugs/Issues
<ul>
<li>Search existing issues: Before reporting a new issue, please search for existing ones that may describe the same problem. This saves time and prevents duplicate work. <br>
<li>Use the issue template: We have a specific issue template to help you provide all the necessary information for us to effectively address the issue. Please fill it out as thoroughly as possible.<br>
<li>Be clear and concise: Describe the issue in a clear and concise way, including:<br>
<li>Steps to reproduce the issue: Provide detailed steps so we can easily replicate the problem.<br>
<li>Expected behavior: Describe what you expect to happen instead of what actually happens.<br>
<li>System information: Include details about your operating system, browser (if applicable), and any relevant software versions.<br>
<li>Screenshots/Video recordings: If possible, include screenshots or video recordings of the issue to make it easier to understand.<br>
</ul>

### Contributing Code
<ul>
<li>Fork the repository: Create your own fork of the repository before you start making changes. This allows you to work on your own branch without affecting the main codebase.<br>
<li>Follow coding standards: Please adhere to the project's coding standards to ensure consistency and maintainability. You can find the coding standards documented in the Coding Standards document: link to coding standards document.<br>
<li>Write unit tests: For any new code you write, please also write unit tests to ensure that your changes don't break anything.<br>
<li>Create a pull request: Once you're happy with your changes, create a pull request to the main repository. Please provide a clear and concise description of your changes in the pull request description.<br>
</ul>