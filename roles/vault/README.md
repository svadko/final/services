# SvaDko
## Short Description
Vault is a secure storage solution designed to safeguard sensitive information, credentials, and secrets. It provides robust encryption, access control, and audit trails.<br>

## Detailed Description
Vault is an essential component for any modern application or infrastructure. It serves the following purposes:<br>

Secret Management: The vault securely stores secrets such as API keys, passwords, tokens, and certificates. These secrets are critical for application functionality and must be protected from unauthorized access.<br>

Encryption: The vault encrypts sensitive data at rest, ensuring that even if the underlying storage is compromised, the secrets remain confidential.<br>

Access Control: Access to the vault is tightly controlled. Only authorized users or applications with proper credentials can retrieve secrets. Role-based access ensures that different teams or services have appropriate permissions.<br>

Auditing and Logging: The vault maintains detailed logs of all access and modification events. This audit trail is crucial for compliance and troubleshooting.<br>

## Responsible People
20B030614 Keulimzhanov_Magzhan - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030498 Pirniyazov_Miras - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030587 Zhaksylykov_Nursultan - Nomad, Vault, GitLab Runner, Gitlab CI/CD<br />
21B030690 Kusanov_Ramzes - Nomad, Vault, GitLab Runner, Gitlab CI/CD, Logging and monitoring<br />
20B030668 Kaliev_Alikhan - Traefik Proxy, GitLab Runner, Gitlab CI/CD<br />

## Installation
Follow these steps to install and run Vault:<br>

### Prerequisites:<br>

Ensure you have Ansible installed on your system.<br>
Prepare an inventory file (in.yaml) with the necessary server details.<br>
Run Ansible Playbook: Execute the following command to install the vault:<br>

ansible-playbook -i in.yaml vault.yml -e "vault_version=1.15.3"<br>

Access the Vault: Once the installation is complete, access the vault via its web interface or API endpoints.<br>

## TODO
Enhance secret rotation mechanisms.<br>
Implement fine-grained access policies.<br>
Explore integration with cloud-based key management services.<br>

## Contributing
### Reporting Bugs/Issues
<ul>
<li>Search existing issues: Before reporting a new issue, please search for existing ones that may describe the same problem. This saves time and prevents duplicate work. <br>
<li>Use the issue template: We have a specific issue template to help you provide all the necessary information for us to effectively address the issue. Please fill it out as thoroughly as possible.<br>
<li>Be clear and concise: Describe the issue in a clear and concise way, including:<br>
<li>Steps to reproduce the issue: Provide detailed steps so we can easily replicate the problem.<br>
<li>Expected behavior: Describe what you expect to happen instead of what actually happens.<br>
<li>System information: Include details about your operating system, browser (if applicable), and any relevant software versions.<br>
<li>Screenshots/Video recordings: If possible, include screenshots or video recordings of the issue to make it easier to understand.<br>
</ul>

### Contributing Code
<ul>
<li>Fork the repository: Create your own fork of the repository before you start making changes. This allows you to work on your own branch without affecting the main codebase.<br>
<li>Follow coding standards: Please adhere to the project's coding standards to ensure consistency and maintainability. You can find the coding standards documented in the Coding Standards document: link to coding standards document.<br>
<li>Write unit tests: For any new code you write, please also write unit tests to ensure that your changes don't break anything.<br>
<li>Create a pull request: Once you're happy with your changes, create a pull request to the main repository. Please provide a clear and concise description of your changes in the pull request description.<br>
</ul>