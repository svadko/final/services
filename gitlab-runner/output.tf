output "gitlab_runner_token" {
  sensitive = true
  value = gitlab_user_runner.devrunner.token
}
