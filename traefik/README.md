# SvaDko
## Short Description
SvaDko is an online e-shop that offers a wide range of products. In our project, we utilize Traefik, a modern HTTP reverse proxy and load balancer.<br> Traefik makes deploying microservices easy by integrating with existing
infrastructure components such as Docker, Swarm mode, Kubernetes, Consul, Etcd, Rancher v2, and Amazon ECS.<br> It configures itself automatically and dynamically, allowing seamless connectivity for your microservices.<br>

## Detailed Description
Vault is an essential component for any modern application or infrastructure. It serves the following purposes:<br>

Secret Management: The vault securely stores secrets such as API keys, passwords, tokens, and certificates. These secrets are critical for application functionality and must be protected from unauthorized access.<br>

Encryption: The vault encrypts sensitive data at rest, ensuring that even if the underlying storage is compromised, the secrets remain confidential.<br>

Access Control: Access to the vault is tightly controlled. Only authorized users or applications with proper credentials can retrieve secrets. Role-based access ensures that different teams or services have appropriate permissions.<br>

Auditing and Logging: The vault maintains detailed logs of all access and modification events. This audit trail is crucial for compliance and troubleshooting.<br>

## Responsible People
20B030614 Keulimzhanov_Magzhan - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030498 Pirniyazov_Miras - Pre-Setup, GitLab CI/CD, Logging and Monitoring<br />
20B030587 Zhaksylykov_Nursultan - Nomad, Vault, GitLab Runner, Gitlab CI/CD<br />
21B030690 Kusanov_Ramzes - Nomad, Vault, GitLab Runner, Gitlab CI/CD, Logging and monitoring<br />
20B030668 Kaliev_Alikhan - Traefik Proxy, GitLab Runner, Gitlab CI/CD<br />

## Installation
Follow these steps to install and run the SvaDko project:<br>

### Clone the Repository:

git clone https://github.com/SvaDko/SvaDko.git<br>
cd SvaDko<br>

### Configure Traefik:

Ensure you have Docker running.<br>
Refer to the 5-Minute Quickstart in the Traefik documentation.<br>
Set Environment Variables:<br>

### Create a .env file with the following content:
GITLAB_GROUP_ID=79699575<br>
GITLAB_TOKEN=glpat-yhNMrYMkCsMN3ATTqX7u<br>
ADDRESS_NOMAD=http://104.248.250.188:4646<br>

### Run Nomad Job:

Execute the following command to run the Nomad job:<br>
nomad job run -detach -address=http://104.248.250.188:4646 nomad-job.hcl<br>

## TODO
Enhance the product recommendation engine.<br>
Implement real-time inventory tracking.<br>
Integrate with third-party shipping services.<br>

## Contributing
### Reporting Bugs/Issues
<ul>
<li>Search existing issues: Before reporting a new issue, please search for existing ones that may describe the same problem. This saves time and prevents duplicate work. <br>
<li>Use the issue template: We have a specific issue template to help you provide all the necessary information for us to effectively address the issue. Please fill it out as thoroughly as possible.<br>
<li>Be clear and concise: Describe the issue in a clear and concise way, including:<br>
<li>Steps to reproduce the issue: Provide detailed steps so we can easily replicate the problem.<br>
<li>Expected behavior: Describe what you expect to happen instead of what actually happens.<br>
<li>System information: Include details about your operating system, browser (if applicable), and any relevant software versions.<br>
<li>Screenshots/Video recordings: If possible, include screenshots or video recordings of the issue to make it easier to understand.<br>
</ul>

### Contributing Code
<ul>
<li>Fork the repository: Create your own fork of the repository before you start making changes. This allows you to work on your own branch without affecting the main codebase.<br>
<li>Follow coding standards: Please adhere to the project's coding standards to ensure consistency and maintainability. You can find the coding standards documented in the Coding Standards document: link to coding standards document.<br>
<li>Write unit tests: For any new code you write, please also write unit tests to ensure that your changes don't break anything.<br>
<li>Create a pull request: Once you're happy with your changes, create a pull request to the main repository. Please provide a clear and concise description of your changes in the pull request description.<br>
</ul>

